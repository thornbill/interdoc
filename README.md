# 🚀 Interdoc - The intergalactic document converter

Interdoc is an API and minimal web interface to convert documents between different formats. The project uses [Pandoc](https://pandoc.org/) for conversion with some improved handling of markdown.

A demo is available at [interdoc.thornbill.dev](https://interdoc.thornbill.dev).

## Running Interdoc

* With node:

  > NOTE: Pandoc must be installed and available in your PATH.

  ```sh
  npm i
  npm start
  ```

* With docker:

  ```sh
  docker run -p 8080:8080 --name interdoc registry.gitlab.com/thornbill/interdoc:latest
  ```
