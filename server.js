const bodyParser = require("body-parser")
const express = require("express")
const helmet = require("helmet")
const morgan = require("morgan")

const Converter = require("./util/converter")
const Formats = require("./util/formats")

const DEFAULT_PORT = 8080

// initialize the express app
const app = express()

// add logging
app.use(morgan("short"))

// use helmet middleware for security
app.use(helmet())

// add public/ as a static path
app.use(express.static("public"))

// parse the request body
app.use(bodyParser.text())
app.use(bodyParser.urlencoded({ extended: false }))

const converterHandler = function(req, res, html) {
	let format = req.params.format
	if (!format) {
		if (req.body && req.body.format) {
			format = req.body.format
		} else if (req.accepts(Formats.HTML.filetype)) {
			format = Formats.HTML.format
		} else if (req.accepts(Formats.PDF.filetype)) {
			format = Formats.PDF.format
		} else if (req.accepts(Formats.DOCX.filetype)) {
			format = Formats.DOCX.format
		} else if (req.accepts(Formats.TEXT.filetype)) {
			format = Formats.TEXT.format
		}
	}

	// if format or content to convert is missing return 400
	if (!format || !html) {
		return res.sendStatus(400)
	}

	const fileConverter = new Converter(html)
	fileConverter.convert(format)
		.then((converted) => {
			if (Formats.Binary.indexOf(format) > -1) {
				res.set({
					"Content-Disposition": `attachment; filename=interdoc.${converted.fileType}`
				})
			}
			res.type(converted.fileType)
			res.send(converted.content)
		})
		.catch((ex) => {
			console.error(ex) // eslint-disable-line no-console
			res.sendStatus(500)
		})
}

app.post(["/api/download", "/api/download/:format"], (req, res) => {
	if (!req.body) return res.sendStatus(400)
	const html = typeof req.body.text != "undefined" ? req.body.text : req.body
	converterHandler(req, res, html)
})

// start the server
const port = process.env.PORT || DEFAULT_PORT
app.listen(port, () => {
	console.log(`🚀 InterDoc server listening at http://localhost:${port}`) // eslint-disable-line no-console
})
