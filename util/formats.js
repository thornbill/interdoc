const Formats = {
	ASCIIDOC: {
		format: "asciidoc",
		filetype: "txt"
	},
	BEAMER: {
		format: "beamer",
		filetype: "tex"
	},
	COMMONMARK: {
		format: "commonmark",
		filetype: "md"
	},
	CONTEXT: {
		format: "context",
		filetype: "cld"
	},
	DOCBOOK: {
		format: "docbook",
		filetype: "dbk"
	},
	DOCBOOK5: {
		format: "docbook5",
		filetype: "dbk"
	},
	DOCX: {
		format: "docx",
		filetype: "docx"
	},
	DOKUWIKI: {
		format: "dokuwiki",
		filetype: "txt"
	},
	DZSLIDES: {
		format: "dzslides",
		filetype: "html"
	},
	EPUB: {
		format: "epub",
		filetype: "epub"
	},
	EPUB3: {
		format: "epub3",
		filetype: "epub"
	},
	FICTIONBOOK2: {
		format: "fb2",
		filetype: "fb2"
	},
	HADDOCK: {
		format: "haddock",
		filetype: "txt"
	},
	HTML: {
		format: "html",
		filetype: "html"
	},
	HTML5: {
		format: "html5",
		filetype: "html"
	},
	ICML: {
		format: "icml",
		filetype: "icml"
	},
	JSON: {
		format: "json",
		filetype: "json"
	},
	LATEX: {
		format: "latex",
		filetype: "tex"
	},
	MANPAGES: {
		format: "man",
		filetype: "man"
	},
	MARKDOWN: {
		format: "markdown",
		filetype: "md"
	},
	MARKDOWN_GITHUB: {
		format: "markdown_github",
		filetype: "md"
	},
	MARKDOWN_MMD: {
		format: "markdown_mmd",
		filetype: "md"
	},
	MARKDOWN_PHPEXTRA: {
		format: "markdown_phpextra",
		filetype: "md"
	},
	MARKDOWN_STRICT: {
		format: "markdown_strict",
		filetype: "md"
	},
	MEDIAWIKI: {
		format: "mediawiki",
		filetype: "txt"
	},
	NATIVE: {
		format: "native",
		filetype: "hs"
	},
	ODT: {
		format: "odt",
		filetype: "odt"
	},
	OPENDOCUMENT: {
		format: "opendocument",
		filetype: "odt"
	},
	OPML: {
		format: "opml",
		filetype: "opml"
	},
	ORG_MODE: {
		format: "org",
		filetype: "org"
	},
	PDF: {
		format: "pdf",
		filetype: "pdf"
	},
	TEXT: {
		format: "plain",
		filetype: "txt"
	},
	REVEALJS: {
		format: "revealjs",
		filetype: "html"
	},
	RESTRUCTUREDTEXT: {
		format: "rst",
		filetype: "rst"
	},
	RTF: {
		format: "rtf",
		filetype: "rtf"
	},
	S5: {
		format: "s5",
		filetype: "html"
	},
	SLIDEOUS: {
		format: "slideous",
		filetype: "html"
	},
	SLIDY: {
		format: "slidy",
		filetype: "html"
	},
	TEI: {
		format: "tei",
		filetype: "tei"
	},
	TEXINFO: {
		format: "texinfo",
		filetype: "texi"
	},
	TEXTILE: {
		format: "textile",
		filetype: "textile"
	},
	ZIMWIKI: {
		format: "zimwiki",
		filetype: "txt"
	},
	Binary: [
		"docx",
		"epub",
		"epub3",
		"odt",
		"pdf"
	]
}

module.exports = Formats