const crypto = require("crypto")
const marked = require("marked")
const sanitizeHtml = require("sanitize-html")
const fs = require("fs")

const Pandoc = require("./pandoc")
const Formats = require("./formats")

// promisified readFile
const readFileAsync = function(path) {
	return new Promise((resolve, reject) => {
		fs.readFile(path, (err, data) => {
			if (err) return reject(err)
			resolve(data)
		})
	})
}

// promisified unlink
const unlinkAsync = function(path) {
	return new Promise((resolve, reject) => {
		fs.unlink(path, (err) => {
			if (err) return reject(err)
			resolve()
		})
	})
}

const getFileType = function(format) {
	for (const f in Formats) {
		if (Formats[f].format && Formats[f].format === format) {
			return Formats[f].filetype
		}
	}
}

// returns a sha 256 hash of data
const hasher = function(data) {
	const hash = crypto.createHash("sha256")
	hash.update(data)
	return hash.digest("hex")
}

// returns a sanitized html string for a markdown string
const toSafeHtml = function(markdownString) {
	if (!markdownString) return ""
	return sanitizeHtml(
		marked(markdownString),
		{ allowedTags: sanitizeHtml.defaults.allowedTags.concat(["h1", "h2"]) }
	)
}

class Converter {
	constructor(dirtyMd) {
		this.html = toSafeHtml(dirtyMd)
	}

	convert(format) {
		if (Formats.Binary.indexOf(format) > -1) {
			const fileType = getFileType(format)
			const path = `/tmp/${this.getHash()}.${fileType}`
			return Pandoc.convert(format, this.html, path)
				.then(() => readFileAsync(path))
				.then((data) => {
					return {
						fileType: fileType,
						content: data
					}
				})
				.then((response) => {
					// attempt to delete the file swallowing and logging any errors
					unlinkAsync(path)
						.catch((err) => console.error(err)) // eslint-disable-line no-console
					return response
				})
		} else if (format == Formats.HTML.format) {
			return Promise.resolve({
				fileType: Formats.HTML.filetype,
				content: this.html
			})
		} else {
			return Pandoc.convert(format, this.html)
				.then((out) => {
					return {
						fileType: getFileType(format),
						content: out
					}
				})
		}
	}

	getHash() {
		if (!this.hash) this.hash = hasher(this.html)
		return this.hash
	}
}

module.exports = Converter