const spawn = require("child_process").spawn

// spawns the pandoc command
const spawnPandoc = function(opts, input) {
	return new Promise((resolve, reject) => {
		if (!input) return reject(new Error("Failed to provide input to Pandoc"))

		let stdout = "", stderr = ""
		// spawn pandoc
		const pandoc = spawn("pandoc", opts)
		// handle bad command
		pandoc.on("error", reject)
		// write input to stdin
		pandoc.stdin.end(input)

		// store data on stdout and stderr
		pandoc.stdout.on("data", (data) => stdout += data)
		pandoc.stderr.on("data", (data) => stderr += data)

		// handle errors or return data on close
		pandoc.on("close", (code) => {
			if (code !== 0) return reject(new Error(`pandoc exited with code ${code}: ${stderr}`))
			resolve(stdout)
		})
	})
}

module.exports = {
	convert(format="plain", input, filename) {
		// pdf output must be written to a file and do not support the --to option
		if (format == "pdf") {
			return spawnPandoc(["--from", "html", "--output", filename], input)
		}
		// odt, docx, epub, and epub3 output must be written to a file
		else if (format == "odt" || format == "docx" || format == "epub" || format == "epub3") {
			return spawnPandoc(["--from", "html", "--to", format, "--output", filename], input)
		}
		// any other output can be written to stdout
		else {
			return spawnPandoc(["--from", "html", "--to", format], input)
		}
	}
}
