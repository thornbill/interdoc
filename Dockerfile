FROM node:16

RUN set -ex; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		pandoc \
		texlive-latex-base \
		texlive-fonts-recommended \
		lmodern \
	;

RUN mkdir -p /var/interdoc
WORKDIR /var/interdoc

COPY . /var/interdoc
RUN npm ci

USER node
EXPOSE 8080
CMD ["node", "server.js"]
